# coding: utf-8
import numpy as np
import torch
from torch import from_numpy
import re
import json
import os


def tokenize_en(text):
    text = text.replace('a.m.', 'AM').replace('"', ' " ')
    text = text.replace('.', ' .').replace('!', ' !')
    text = text.replace('?', ' ? ').replace("'", " '")
    text = text.replace(',', ' , ').replace("’", " '")
    text = text.replace('‘', "' ").replace("´", "' ")
    text = text.replace('«', ' " ').replace('»', ' " ')
    text = text.replace('“', ' " ').replace('”', ' " ')
    text = text.replace('(', ' ( ').replace(')', ' ) ')
    text = text.replace('{', ' { ').replace('}', ' } ')
    text = text.replace('[', ' [ ').replace(']', ' ] ')
    text = text.replace('…', ' … ')
    text = re.sub(r"\s+", " ", text)
    return text.split()


def tokenize_fr(text):
    text = text.replace("'", "' ").replace('’', "' ")
    text = text.replace('‘', "' ").replace("´", "' ")
    text = text.replace("-t-", " -t- ").replace("-", " - ")
    text = text.replace(',', ' , ').replace('.', ' . ')
    text = text.replace('?', ' ? ').replace('!', ' ! ')
    text = text.replace('(', ' ( ').replace(')', ' ) ')
    text = text.replace('{', ' { ').replace('}', ' } ')
    text = text.replace('[', ' [ ').replace(']', ' ] ')
    text = text.replace('«', ' " ').replace('»', ' " ')
    text = text.replace('“', ' " ').replace('”', ' " ')
    text = text.replace('"', ' " ').replace('…', ' … ')
    text = re.sub(r"\s+", " ", text)
    return text.split()


def process_data(args, encoding='utf8'):
    with open(args.src, 'r', encoding=encoding) as f:
        args.src_str_list = f.read().strip().split('\n')[:10000]
    args.train_len = len(args.src_str_list)
    args.batch_count = args.train_len / args.batch_size
    print('Number of examples :', len(args.src_str_list))

    args.src_tokens = [tokenize_en(t) for t in args.src_str_list]

    args.pad_id = 0
    args.unk_id = 1
    args.sos_id = 2
    args.eos_id = 3
    args.mask_id = 4
    vocab_path = os.path.join('data', 'vocab.json')
    if os.path.exists(vocab_path):
        with open(vocab_path, 'r') as f:
            vocab = json.load(f)
            args.src_t2id = vocab['src_t2id']
            args.src_id2t = vocab['src_id2t']
            args.token_count = vocab['token_count']
            args.total_tokens = vocab['total_tokens']
    else:
        args.src_t2id = {}
        args.src_t2id['<pad>'] = args.pad_id
        args.src_t2id['<unk>'] = args.unk_id
        args.src_t2id['<sos>'] = args.sos_id
        args.src_t2id['<eos>'] = args.eos_id
        args.src_t2id['<mask>'] = args.mask_id
        args.token_count = {}
        args.total_tokens = 0
        cnt = 5
        for seq in args.src_tokens:
            for t in seq:
                if t not in args.src_t2id.keys():
                    args.src_t2id[t] = cnt
                    cnt += 1
                if t not in args.token_count.keys():
                    args.token_count[t] = 1
                else:
                    args.token_count[t] += 1
                args.total_tokens += 1
        args.src_id2t = {str(v): k for k, v in args.src_t2id.items()}
        vocab = {}
        vocab['src_t2id'] = args.src_t2id
        vocab['src_id2t'] = args.src_id2t
        vocab['token_count'] = args.token_count
        vocab['total_tokens'] = args.total_tokens
        with open(vocab_path, 'w') as f:
            json.dump(vocab, f)
    args.src_vocab_size = len(args.src_t2id.keys())
    args.tokens_frequency = np.zeros((args.src_vocab_size,))
    for t, count in args.token_count.items():
        args.tokens_frequency[args.src_t2id[t]] = count
    args.tokens_frequency /= args.total_tokens
    print('Src vocab size :', args.src_vocab_size)
    print('Src total tokens :', args.total_tokens)

    args.src_ids_list = np.array([[args.src_t2id[t] for t in s]
                                  for s in args.src_tokens])
    args.trg_ids_list = [[args.src_t2id[t] for t in s]
                         for s in args.src_tokens]
    # we add <sos> and <eos> tokens at the begining and end of  target sequence
    args.trg_ids_list = np.array([[args.sos_id] + s + [args.eos_id]
                                  for s in args.trg_ids_list])

    src_counts = np.array([len(s) for s in args.src_ids_list])

    args.src_max = np.max(src_counts)
    args.src_min = np.min(src_counts)
    print('Max source sequence length :', args.src_max)
    print('Min source sequence length :', args.src_min)
    print('Max target sequence length (with <sos> and <eos> tokens):',
          args.src_max+2)
    print('Min target sequence length (with <sos> and <eos> tokens) :',
          args.src_min+2)
    args.max_tokens = args.src_max+2
    args.src_mat = np.full((args.train_len, args.src_max), args.pad_id)
    args.trg_mat = np.full((args.train_len, args.src_max+2), args.pad_id)
    # order samples from longest source length to shortest by mean
    sort_ids_desc = np.argsort(src_counts)[::-1]

    for i in range(args.train_len):
        id_ = sort_ids_desc[i]
        args.src_mat[i, :len(args.src_ids_list[id_])] = args.src_ids_list[id_]
        args.trg_mat[i, :len(args.trg_ids_list[id_])] = args.trg_ids_list[id_]

    src_str_temp_list = [args.src_str_list[i] for i in sort_ids_desc]
    args.src_str_list = src_str_temp_list

    src_counts_temp_list = [src_counts[i] for i in sort_ids_desc]
    src_counts = src_counts_temp_list

    src_ids_temp_list = [args.src_ids_list[i] for i in sort_ids_desc]
    args.src_ids_list = np.array(src_ids_temp_list)
    trg_ids_temp_list = [args.trg_ids_list[i] for i in sort_ids_desc]
    args.trg_ids_list = np.array(trg_ids_temp_list)

    return DataIterator(args.src_mat, args.trg_mat,
                        src_counts, args.batch_size,
                        args.src_vocab_size,
                        args.tokens_frequency, args.device)


class DataIterator:
    def __init__(self, src_mat, trg_mat, src_counts,
                 batch_size, src_vocab_size, tokens_frequency,
                 device):
        self.src_mat = src_mat
        self.trg_mat = trg_mat
        self.src_counts = src_counts
        self.batch_size = batch_size
        self.src_vocab_size = src_vocab_size
        self.tokens_frequency = tokens_frequency
        self.device = device
        self.examples_count = src_mat.shape[0]
        self.batch_count = self.examples_count / self.batch_size
        self.current = -1
        self.processed = 0

    def __iter__(self):
        return self

    def __next__(self):
        self.current += 1
        if self.current < self.batch_count:
            if self.current % 2 == 0:
                start = (self.current // 2) * self.batch_size
            else:
                start = - ((self.current + 1) // 2) * self.batch_size
            # last batch is probably not full
            batch_size = min(self.examples_count - self.processed,
                             self.batch_size)
            end = start + batch_size
            if self.current % 2 == 1 and end == 0:
                end = None
            src_max_len = np.max(self.src_counts[start:end])
            batch_mat_src = from_numpy(self.src_mat[start:end, :src_max_len]).long()
            batch_mat_trg = from_numpy(self.trg_mat[start:end, :src_max_len+2]).long()
            self.processed += batch_size
            return (batch_mat_src.to(self.device),
                    batch_mat_trg.to(self.device),
                    self.src_counts[start:end])
        else:
            self.current = -1
            self.processed = 0
            raise StopIteration


def extend_matrix(mat, args):
    pads = from_numpy(np.full((mat.size(0), 1), args.pad_id))
    mat = torch.cat((mat, pads.long().to(args.device)), 1)
    return mat


def mask_source(src, src_counts, args):
    batch_size = src.size(0)
    rand = torch.rand(batch_size, src.size(1))
    mask = (rand < args.mask_prob)
    pad_mask = (src != args.pad_id).cpu()
    mask = (mask & pad_mask)
    src[mask] = args.unk_id
    for j in range(batch_size):
        seg_len = int(src_counts[j])
        rand = np.random.rand(1)
        # cut and swap
        if args.cut_and_swap_prob > 0.0 and rand < args.cut_and_swap_prob:
            rand = torch.LongTensor(1).random_(0, 2*seg_len//3)
            rand += seg_len//3
            seg_1 = src[j, :rand]
            seg_2 = src[j, rand:seg_len]
            src[j, :seg_len] = torch.cat((seg_2, seg_1), 0)
        rand = np.random.rand(1)
        if args.insert_suppr_prob > 0.0 and rand < args.insert_suppr_prob:
            rand = np.random.rand(1)
            # 50 % chance we suppress/insert a token
            if rand < 0.5:
                # suppr
                token_id = np.random.randint(low=0, high=seg_len)
                seg_1 = src[j, :token_id]
                seg_2 = src[j, token_id+1:seg_len]
                token = torch.LongTensor(0).to(args.device)
                token = token.new_full((1,), args.pad_id)
                if seg_1.size(0) == 0:
                    concat = torch.cat((seg_2, token), 0)
                elif seg_2.size(0) == 0:
                    concat = torch.cat((seg_1, token), 0)
                else:
                    concat = torch.cat((seg_1, seg_2, token), 0)
                src[j, :seg_len] = concat
            else:
                # insert
                rand_token_id = np.random.choice(args.src_vocab_size, 1,
                                                 p=args.tokens_frequency)
                pos_id = np.random.randint(low=0, high=seg_len)
                seg_1 = src[j, :pos_id]
                seg_2 = src[j, pos_id:seg_len]
                token = torch.LongTensor(0).to(args.device)
                token = token.new_full((1,), int(rand_token_id))
                concat = torch.cat((seg_1, token, seg_2), 0)
                if concat.size(0) == src.size(1)+1:
                    src = extend_matrix(src, args)
                src[j, :seg_len+1] = concat
    return src
