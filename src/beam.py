# coding: utf-8
import torch
from torch import from_numpy
from src.model import nopeak_mask
# import torch.nn.functional as F
import math
import numpy as np
# from time import time


def init_vars(src, model, args):
    pad = from_numpy(np.full((src.size(0), 1), args.pad_id))
    src = torch.cat((src, pad.long().to(args.device)), 1)
    src_mask = (src != args.pad_id).unsqueeze(-2)
    trg_mask = nopeak_mask(1, args)
    e_output = model(src, src_mask, trg_mask)
    preds_labels = e_output.argmax(dim=2)
    return preds_labels.cpu().numpy()


def k_best_outputs(outputs, out, log_scores, i, k):
    probs, ix = out[:, -1].data.topk(k)
    log_probs = torch.Tensor([math.log(p) for p in
                              probs.data.view(-1)]).view(k, -1)
    log_probs += log_scores.transpose(0, 1)
    k_probs, k_ix = log_probs.view(-1).topk(k)

    row = k_ix // k
    col = k_ix % k

    outputs[:, :i] = outputs[row, :i]
    outputs[:, i] = ix[row, col]

    log_scores = k_probs.unsqueeze(0)

    return outputs, log_scores


def beam_search(src, model, args):
    # t0 = time()
    preds_labels = init_vars(src, model, args)
    print('preds_labels', preds_labels)
    return ' '.join([args.src_id2t[str(tok)]
                     for tok in preds_labels[0, :]])
