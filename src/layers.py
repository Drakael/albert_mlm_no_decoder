# coding: utf-8
import torch.nn as nn


class EncoderLayer(nn.Module):
    def __init__(self, attn, norm, ff, dropout_layer, d_hidden, heads):
        super().__init__()
        self.norm = norm
        self.attn = attn
        self.ff = ff
        self.dropout = dropout_layer

    def forward(self, x, mask):
        x2 = self.norm(x)
        x = x + self.dropout(self.attn(x2, x2, x2, mask))
        x2 = self.norm(x)
        x = x + self.dropout(self.ff(x2))
        return x


# build a decoder layer with two multi-head attention layers and
# one feed-forward layer
class DecoderLayer(nn.Module):
    def __init__(self, attn, norm, ff, dropout_layer, d_hidden, heads):
        super().__init__()
        self.norm = norm
        self.dropout = dropout_layer
        self.attn = attn
        self.ff = ff

    def forward(self, x, e_outputs, src_mask, trg_mask):
        # x2 = self.norm(x)
        # x = x + self.dropout(self.attn(x2, x2, x2, trg_mask))
        x2 = self.norm(x)
        x = x + self.dropout(self.attn(x2, e_outputs, e_outputs,
                             src_mask))
        x2 = self.norm(x)
        x = x + self.dropout(self.ff(x2))
        return x
