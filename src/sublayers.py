# coding: utf-8
import torch
import torch.nn as nn
import math
from src.optim import Gelu


class Norm(nn.Module):
    def __init__(self, d_hidden, eps=1e-6):
        super().__init__()
        self.size = d_hidden
        # create two learnable parameters to calibrate normalisation
        self.alpha = nn.Parameter(torch.ones(self.size))
        self.bias = nn.Parameter(torch.zeros(self.size))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.alpha * (x - mean) / (std + self.eps) + self.bias


def attention(q, k, v, d_k, mask=None, dropout=None):
    scores = torch.matmul(q, k.transpose(-2, -1)) / math.sqrt(d_k)
    if mask is not None:
        # mask.size is batch_size * 1 * seq_len
        mask = mask.unsqueeze(1)
        scores = scores.masked_fill(mask == 0, -1e9)
    scores = nn.Softmax(dim=-1)(scores)
    if dropout is not None:
        scores = dropout(scores)
    # scores dimensions are batch_size * nb_heads * seq_len * seq_len
    output = torch.matmul(scores, v)
    # output dimensions are batch_size * nb_heads * seq_len * d_heads
    return output, scores


class MultiHeadAttention(nn.Module):
    def __init__(self, heads, d_hidden, dropout_layer):
        super().__init__()
        self.d_hidden = d_hidden
        self.d_k = d_hidden // heads
        self.h = heads
        self.linear = nn.Linear(d_hidden, d_hidden)
        self.scores = None
        self.dropout = dropout_layer
        self.out = nn.Linear(d_hidden, d_hidden)

    def forward(self, q, k, v, mask=None):
        bs = q.size(0)
        k = k.long()
        # dimensions are batch_size * seq_len * d_hidden
        # perform linear operation and split into N heads
        k = self.linear(k.float()).view(bs, -1, self.h, self.d_k)
        q = self.linear(q.float()).view(bs, -1, self.h, self.d_k)
        v = self.linear(v.float()).view(bs, -1, self.h, self.d_k)
        # dimensions are batch_size * seq_len * nb_head * d_heads
        # transpose to get dimensions bs * N * sl * d_hidden
        k = k.transpose(1, 2)
        q = q.transpose(1, 2)
        v = v.transpose(1, 2)
        # dimensions are batch_size * nb_head * seq_len * d_heads
        # calculate attention using function we will define next
        output, self.scores = attention(q, k, v, self.d_k, mask, self.dropout)
        # concatenate heads and put through final linear layer
        concat = output.transpose(1, 2).contiguous() \
            .view(bs, -1, self.d_hidden)
        # dimensions are batch_size * seq_len * d_hidden
        output = self.out(concat)
        return output


class FeedForward(nn.Module):
    def __init__(self, d_hidden, dropout_layer, d_ff=None):
        super().__init__()
        if d_ff is None:
            d_ff = 4 * d_hidden
        # We set d_ff as a default to 4 * d_hidden
        self.linear_1 = nn.Linear(d_hidden, d_ff)
        self.linear_2 = nn.Linear(d_ff, d_hidden)
        self.dropout = dropout_layer
        self.activation = Gelu()

    def forward(self, x):
        x = self.dropout(self.activation(self.linear_1(x)))
        x = self.linear_2(x)
        return x
