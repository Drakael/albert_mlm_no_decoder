# coding: utf-8
import argparse
import torch
from src.data import tokenize_en
from src.model import get_model
from src.beam import beam_search
from torch.autograd import Variable
import re
import os
import json


def multiple_replace(dict, text):
    # Create a regular expression  from the dictionary keys
    regex = re.compile("(%s)" % "|".join(map(re.escape, dict.keys())))
    # For each match, look-up corresponding value in dictionary
    return regex.sub(lambda mo: dict[mo.string[mo.start():mo.end()]], text)


def translate_sentence(sentence, model, args):
    model.eval()
    indexed = [args.sos_id]
    sentence = tokenize_en(sentence)
    for tok in sentence:
        t = args.src_t2id[tok]
        if t != args.pad_id:
            indexed.append(t)
    print('sentence', sentence)
    print('indexed', indexed)
    sentence = Variable(torch.LongTensor([indexed]))
    if args.device == 0:
        sentence = sentence.cuda()
    sentence = beam_search(sentence, model, args)
    return multiple_replace({'AM': 'a.m.', ' ?': '?', ' !': '!', ' .': '.',
                             ' \'': '\'', ' ,': ',', ' -t- ': '-t-',
                             ' - ': '-', ' … ': '…'}, sentence)


def translate(args, model):
    sentence = args.text
    translated = []
    if len(sentence) > 0:
        translated.append(translate_sentence(sentence, model, args))
    return ' '.join(translated)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-load_weights', default='weights')
    parser.add_argument('-ckpt_file', default='model_weights_e20_loss13.6324_acc0.9219.ckpt')
    parser.add_argument('-k', type=int, default=2)
    parser.add_argument('-n_layers', type=int, default=12)
    parser.add_argument('-d_vocab', type=int, default=128)
    parser.add_argument('-d_hidden', type=int, default=512)
    parser.add_argument('-heads', type=int, default=8)
    parser.add_argument('-dropout', type=int, default=0.0)
    parser.add_argument('-batchsize', type=int, default=32)
    parser.add_argument('-no_cuda', action='store_true', default=False)
    parser.add_argument('-max_tokens', type=int, default=53)

    args = parser.parse_args()

    args.device = 0 if args.no_cuda is False else -1

    assert args.k > 0
    assert args.max_tokens > 1

    vocab_path = os.path.join('data', 'vocab.json')
    if os.path.exists(vocab_path):
        with open(vocab_path, 'r') as f:
            vocab = json.load(f)
            args.src_t2id = vocab['src_t2id']
            args.src_id2t = vocab['src_id2t']
            args.src_vocab_size = len(args.src_t2id.keys())
            args.pad_id = 0
            args.unk_id = 1
            args.sos_id = 2
            args.eos_id = 3
            args.mask_id = 4
    else:
        quit('No vocab file found at data/vocab.json')
    model = get_model(args)

    while True:
        args.text = input("Enter a sentence to translate (type 'f' to load \
                           from file, or 'q' to quit):\n")
        if args.text == "q":
            break
        if args.text == 'f':
            input("Enter a sentence to translate (type 'f' to load \
                   from file, or 'q' to quit):\n")
            try:
                args.text = ' '.join(open(args.text, encoding='utf8')
                                     .read()
                                     .split('\n'))
            except:
                print("error opening or reading text file")
                continue
        phrase = translate(args, model)
        print('> ' + phrase + '\n')

if __name__ == '__main__':
    main()
